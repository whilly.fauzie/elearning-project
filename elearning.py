from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from datetime import datetime, date
from flask_cors import CORS, cross_origin
import base64

app = Flask(__name__)

app.config['SECRET_KEY']='secret'
app.config['SQLALCHEMY_DATABASE_URI']='postgresql://postgres:Januari2930@localhost:5432/elearning_project'
db = SQLAlchemy(app)
migrate = Migrate(app,db)

class Siswa(db.Model):
    siswa_id = db.Column(db.Integer, primary_key=True, index=True)
    nama = db.Column(db.String(20), nullable=False, unique=True)
    email = db.Column(db.String(30), nullable=False, unique=True)
    password = db.Column(db.String(20), nullable=False)
    jumlah_kelas_berjalan = db.Column(db.Integer, default=0)  # untuk cek maks kelas
    siswa_kursus = db.relationship('Pendaftaran', backref='wasus', lazy='dynamic')

class Tutor(db.Model):
    tutor_id = db.Column(db.Integer, primary_key=True, index=True)
    nama = db.Column(db.String(20), nullable=False, unique=True)
    email = db.Column(db.String(30), nullable=False, unique=True)
    password = db.Column(db.String(20), nullable=False)
    is_admin = db.Column(db.Boolean, default=False)
    tutor_kursus = db.relationship('Kursus', backref='torus', lazy='dynamic')

class Kursus(db.Model):
    kursus_id = db.Column(db.Integer, primary_key=True, index=True)
    tutor_id = db.Column(db.Integer, db.ForeignKey('tutor.tutor_id'), nullable=False)
    kategori = db.Column(db.String, nullable=False)
    nama_kursus = db.Column(db.String(50), nullable=False)
    deskripsi = db.Column(db.String, nullable=False)
    konten = db.Column(db.String, nullable=False)
    syarat = db.Column(db.String, nullable=False)
    kelas_kursus = db.relationship('Pendaftaran', backref='asus', lazy='dynamic')

class Pendaftaran(db.Model):
    registrasi_id = db.Column(db.Integer, primary_key=True, index=True)
    siswa_id = db.Column(db.Integer, db.ForeignKey('siswa.siswa_id'), nullable=False)
    kursus_id = db.Column(db.Integer, db.ForeignKey('kursus.kursus_id'), nullable=False)
    tanggal_pendaftaran = db.Column(db.DateTime, default=datetime.now, nullable=False)
    tanggal_penyelesaian = db.Column(db.DateTime)
    dropout = db.Column(db.DateTime)


@app.route('/login/', methods=['POST'])
def login_user():
    header = request.headers.get('authorization')
    plain = base64.b64decode(header[6:]).decode('utf-8')
    planb = plain.split(":")
    user = Siswa.query.filter_by(email=planb[0], password=planb[1]).first()
    if user is None:
        return {'error': 'email atau password salah'}, 400
    return jsonify({
        "email": user.email
    }), 201


@app.route('/users/')
def get_users():
    return jsonify([
        {
            'siswa id': siswa.siswa_id, 'nama': siswa.nama, 'email': siswa.email
        } for siswa in Siswa.query.all()
    ])

@app.route('/users/<id>/')
def get_user(id):
    user = Siswa.query.filter_by(siswa_id=id).first_or_404()
    return {
        'siswa id': user.siswa_id, 'nama': user.nama, 'email': user.email
    }

@app.route('/users/', methods=['POST'])
def create_user():
    data = request.get_json()
    # if (not 'nama' in data) or (not 'email' in data):
    #     return jsonify({
    #         'error': 'nama dan email tidak ada'
    #     }), 400
    s = Siswa(
            nama=data['nama'],
            email=data['email'],
            password=data['password']
        )
    db.session.add(s)
    db.session.commit()
    return {
        'sukses': 'data siswa berhasil ditambahkan'
    }, 201

@app.route('/users/<id>/', methods=['PUT'])
def update_user(id):
    data = request.get_json()
    user = Siswa.query.filter_by(siswa_id=id).first_or_404()
    if 'nama' in data:
        user.nama = data['nama'],
    if 'email' in data:
        user.email = data['email'],
    if 'password' in data:
        user.password = data['password']
    db.session.commit()
    return jsonify({
        'sukses': 'data berhasil diubah',
        'siswa id': user.siswa_id, 'nama': user.nama, 'email': user.email
    })

@app.route('/tutors/')
def get_tutors():
    return jsonify([
        {
            'tutor id': tutor.tutor_id, 'nama': tutor.nama, 'email': tutor.email, 'is admin': tutor.is_admin
        } for tutor in Tutor.query.all()
    ])

@app.route('/tutors/<id>/')
def get_tutor(id):
    user = Tutor.query.filter_by(tutor_id=id).first_or_404()
    return {
        'tutor id': user.tutor_id, 'nama': user.nama, 'email': user.email, 'is admin': user.is_admin
    }

@app.route('/tutors/', methods=['POST'])
def create_tutor():
    data = request.get_json()
    if not 'nama' in data or not 'email' in data:
        return jsonify({
            'error': 'nama dan email tidak ada'
        }), 400
    if not 'is_admin' in data:
        t = Tutor(
                nama = data['nama'],
                email = data['email'],
                password = data['password']
            )
        db.session.add(t)
        db.session.commit()
        return {
            'sukses': 'data tutor berhasil ditambahkan',
            'tutor id': t.tutor_id, 'nama': t.nama, 'email': t.email
        }, 201
    else:
        t = Tutor(
                nama = data['nama'],
                email = data['email'],
                password = data['password'],
                is_admin = data['is_admin']
            )
        db.session.add(t)
        db.session.commit()
        return {
            'sukses': 'data tutor berhasil ditambahkan',
            'tutor id': t.tutor_id, 'nama': t.nama, 'email': t.email, 'is admin': t.is_admin
        }, 201

@app.route('/tutors/<id>/', methods=['PUT'])
def update_tutor(id):
    data = request.get_json()
    user = Tutor.query.filter_by(tutor_id=id).first_or_404()
    if 'nama' in data:
        user.nama = data['nama'],
    if 'email' in data:
        user.email = data['email'],
    if 'password' in data:
        user.password = data['password'],
    if 'is_admin' in data:
        user.is_admin = data['is_admin']
    db.session.commit()
    return jsonify({
        'sukses': 'data berhasil diubah',
        'tutor id': user.tutor_id, 'nama': user.nama, 'email':user.email, 'is_admin': user.is_admin
    })

@app.route('/tutors/<id>/', methods=['DELETE'])
def delete_tutor(id):
    tutor = Tutor.query.filter_by(tutor_id=id).first_or_404()
    db.session.delete(tutor)
    db.session.commit()
    return {
        'sukses': 'data berhasil dihapus'
    }

@app.route('/kursus/')
def get_kursus():
    return jsonify([
        {
            '1. kursus id': kursus.kursus_id,
            '2. nama tutor': kursus.torus.nama,
            '3. kategori': kursus.kategori,
            '4. nama kursus': kursus.nama_kursus,
            '5. deskripsi': kursus.deskripsi,
            '6. konten': kursus.konten,
            '7. syarat': kursus.syarat
        }for kursus in Kursus.query.all()
    ])

@app.route('/kursus/<id>/')
def get_kursuss(id):
    k = Kursus.query.filter_by(kursus_id=id).first_or_404()
    return{
        '1. nama tutor': k.torus.nama,
        '2. kategori': k.kategori,
        '3. nama kursus': k.nama_kursus,
        '4. deskripsi': k.deskripsi,
        '5. konten': k.konten,
        '6. syarat': k.syarat
    }

@app.route('/kursus/', methods=['POST'])
def add_kursus():
    data = request.get_json()
    header = request.headers.get('authorization')
    plain = base64.b64decode(header[6:]).decode('utf-8')
    planb = plain.split(":")
    user = Tutor.query.filter_by(email=planb[0], password=planb[1]).first()
    if user is None:
        return {'error': 'email atau password salah'}, 400
    else:
        if planb[0] in user.email and planb[1] in user.password:
            if not 'tutor_id' in data:
                return {'message': 'tutor id tidak ada'}, 400
            if not 'kategori' in data:
                return {'message': 'kategori tidak ada'}, 400
            if not 'nama_kursus' in data:
                return {'message': 'nama kursus tidak ada'}, 400
            if not 'deskripsi' in data:
                return {'message': 'deskripsi tidak ada'}, 400
            if not 'konten' in data:
                return {'message': 'konten tidak ada'}, 400
            if not 'syarat' in data:
                return {'message': 'syarat tidak ada'}, 400
            kursus = Kursus(
                    tutor_id = data['tutor_id'],
                    kategori = data['kategori'],
                    nama_kursus = data['nama_kursus'],
                    deskripsi = data['deskripsi'],
                    konten = data['konten'],
                    syarat = data['syarat']
                )
            db.session.add(kursus)
            db.session.commit()
            return {
                '1. kursus id': kursus.kursus_id,
                '2. tutor id': kursus.tutor_id,
                '3. kategori': kursus.kategori,
                '4. nama kursus': kursus.nama_kursus,
                '5. deskripsi': kursus.deskripsi,
                '6. konten': kursus.konten,
                '7. syarat': kursus.syarat
            }, 201

@app.route('/kursus/<id>/', methods=['PUT'])
def update_kursus(id):
    data = request.get_json()
    header = request.headers.get('authorization')
    plain = base64.b64decode(header[6:]).decode('utf-8')
    planb = plain.split(":")
    user = Tutor.query.filter_by(email=planb[0], password=planb[1]).first()
    if user is None:
        return {'error': 'email atau password salah'}, 400
    else:
        if planb[0] in user.email and planb[1] in user.password:
            k = Kursus.query.filter_by(kursus_id=id).first_or_404()
            if 'tutor_id' in data:
                k.tutor_id = data['tutor_id'],
            if 'kategori' in data:
                k.kategori = data['kategori'],
            if 'nama_kursus' in data:
                k.nama_kursus = data['nama_kursus'],
            if 'deskripsi' in data:
                k.deskripsi = data['deskripsi'],
            if 'konten' in data:
                k.konten = data['konten'],
            if 'syarat' in data:
                k.syarat = data['syarat']
            db.session.commit()
            return jsonify({
                '1. kursus id': kursus.kursus_id,
                '2. tutor id': kursus.tutor_id,
                '3. kategori': kursus.kategori,
                '4. nama kursus': kursus.nama_kursus,
                '5. deskripsi': kursus.deskripsi,
                '6. konten': kursus.konten,
                '7. syarat': kursus.syarat
            })

@app.route('/kursus/<id>/', methods=['DELETE'])
def delete_kursus(id):
    kursus = Kursus.query.filter_by(kursus_id=id).first_or_404()
    db.session.delete(kursus)
    db.session.commit()
    return {
        'sukses': 'data berhasil dihapus'
    }

@app.route('/pendaftaran/')
def get_enroll():
    header = request.headers.get('authorization')
    plain = base64.b64decode(header[6:]).decode('utf-8')
    planb = plain.split(":")
    user = Siswa.query.filter_by(email=planb[0], password=planb[1]).first()
    if user is None:
        return {'error': 'email atau password salah'}, 400
    else:
        if planb[0] in user.email and planb[1] in user.password:
            return jsonify([
                {
                    'siswa': {
                        'siswa id': en.wasus.siswa_id,
                        'nama': en.wasus.nama,
                        'email': en.wasus.email
                    },
                    'Kelas': {
                        'kursus id': en.asus.kursus_id,
                        'nama kursus': en.asus.nama_kursus,
                        'tutor id': en.asus.tutor_id
                    },
                    'registrasi id': en.registrasi_id,
                    'tanggal daftar': en.tanggal_pendaftaran,
                    'tanggal selesai': en.tanggal_penyelesaian,
                    'tanggal drop': en.dropout
                } for en in Pendaftaran.query.all()
            ])

@app.route('/pendaftaran/<id>/')
def get_enrollid(id):
    header = request.headers.get('authorization')
    plain = base64.b64decode(header[6:]).decode('utf-8')
    planb = plain.split(":")
    user = Siswa.query.filter_by(email=planb[0], password=planb[1]).first()
    if user is None:
        return {'error': 'email atau password salah'}, 400
    else:
        if planb[0] in user.email and planb[1] in user.password:
            en = Pendaftaran.query.filter_by(registrasi_id=id).first_or_404()
            return jsonify([
                {
                    'siswa': {
                        'siswa id': en.wasus.siswa_id,
                        'nama': en.wasus.nama,
                        'email': en.wasus.email
                    },
                    'Kelas': {
                        'kursus id': en.asus.kursus_id,
                        'nama kursus': en.asus.nama_kursus,
                        'tutor id': en.asus.tutor_id
                    },
                    'registrasi id': en.registrasi_id,
                    'tanggal daftar': en.tanggal_pendaftaran,
                    'tanggal selesai': en.tanggal_penyelesaian,
                    'tanggal drop': en.dropout
                }
            ])

@app.route('/pendaftaran/', methods=['POST'])
def enroll_class():
    data = request.get_json()
    header = request.headers.get('authorization')
    plain = base64.b64decode(header[6:]).decode('utf-8')
    planb = plain.split(":")
    siswa = Siswa.query.filter_by(siswa_id=data['siswa_id']).first_or_404()
    user = Siswa.query.filter_by(email=planb[0], password=planb[1]).first()
    if user is None:
        return {'error': 'email atau password salah'}, 400
    else:
        if planb[0] in user.email and planb[1] in user.password:
            if not 'siswa_id' in data:
                return {'message': 'siswa id belum terisi'}, 400
            if not 'kursus_id' in data:
                return {'message': 'kursus id belum terisi'}, 400
            if siswa.jumlah_kelas_berjalan > 2:
                return {'error': 'batas maksimum mengikuti kelas hanya 3 kelas'}, 400
            else:
                siswa.jumlah_kelas_berjalan += 1
                enroll = Pendaftaran(
                        siswa_id = data['siswa_id'],
                        kursus_id = data['kursus_id'],
                    )
                db.session.add(enroll)
                db.session.commit()
                return {
                    'success': 'join class',
                    'nama kursus': enroll.asus.nama_kursus
                    }, 201

@app.route('/pendaftaran/<id>/', methods=['PUT'])
def end_class(id):
    data = request.get_json()
    header = request.headers.get('authorization')
    plain = base64.b64decode(header[6:]).decode('utf-8')
    planb = plain.split(":")
    user = Siswa.query.filter_by(email=planb[0], password=planb[1]).first()
    if user is None:
        return {'error': 'email atau password salah'}, 400
    else:
        if planb[0] in user.email and planb[1] in user.password:
            close = Pendaftaran.query.filter_by(registrasi_id=id).first_or_404()
            if not 'tanggal_penyelesaian' and not 'dropout' in data:
                return jsonify({
                    'error': 'tanggal penyelesaian atau tanggal drop tidak ada'
                }), 400
            if 'tanggal_penyelesaian' in data:
                user.jumlah_kelas_berjalan -= 1
                close.tanggal_penyelesaian = data['tanggal_penyelesaian']
                db.session.commit()
                return {
                    'kelas': close.asus.nama_kursus,
                    'message': 'selamat anda telah menyelesaikan kelas'
                }, 201
            if 'dropout' in data:
                user.jumlah_kelas_berjalan -= 1
                close.dropout = data['dropout']
                db.session.commit()
                return {
                    'kelas': close.asus.nama_kursus,
                    'message': 'anda berhasil berhenti dari kelas'
                }, 201


@app.route('/pendaftaran/kursus/<id>/')
def get_enrolluser(id):
    en = Pendaftaran.query.filter_by(kursus_id=id)
    return jsonify([
        {   
            'siswa id': enroll.wasus.siswa_id,
            'nama': enroll.wasus.nama,
            'email': enroll.wasus.email
        } for enroll in (en)
    ])


@app.route('/kursus/search/')
def search_kursus():
    kursus = request.args.get('kursus')
    search = f'%{kursus}%'
    se = Kursus.query.filter(Kursus.nama_kursus.ilike(search))
    return jsonify([
        {
            '1. nama kursus': res.nama_kursus,
            '2. deskripsi': res.deskripsi,
            '3. konten': res.konten,
            '4. syarat': res.syarat
        }for res in (se)
    ])

@app.route('/kursus/top/')
def get_topkursus():
    top = db.session.query(Kursus.nama_kursus).join(Pendaftaran, Kursus.kursus_id == Pendaftaran.kursus_id).group_by(Kursus.nama_kursus).order_by(db.func.count(Kursus.nama_kursus).desc()).limit(2).all()
    return jsonify([
        {
            'nama kursus': course.nama_kursus          
        }for course in (top)
    ])


@app.route('/kursus/topuser/')
def get_topuser():
    top = db.session.query(Siswa.nama).join(Pendaftaran, Siswa.siswa_id == Pendaftaran.siswa_id).group_by(Siswa.nama).order_by(db.func.count(Pendaftaran.tanggal_penyelesaian).desc()).limit(2).all()
    return jsonify([
        {
            'nama siswa': user.nama
        }for user in (top)
    ])


@app.after_request
def after_request_func(response):
    origin = request.headers.get('Origin')
    if request.method == 'OPTIONS':
        response.headers.add('Access-Control-Allow-Credentials', 'true')
        response.headers.add('Access-Control-Allow-Headers', 'Content-Type')
        response.headers.add('Access-Control-Allow-Headers', 'x-csrf-token')
        response.headers.add('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Accept, authorization, Token')
        response.headers.add('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
        if origin:
            response.headers.add('Access-Control-Allow-Origin', origin)
    else:
        response.headers.add('Access-Control-Allow-Credentials', 'true')
        if origin:
            response.headers.add('Access-Control-Allow-Origin', origin)
    return response